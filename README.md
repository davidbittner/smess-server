![logo](https://gitlab.com/davidbittner/smess-server/raw/master/assets/logo.png)

# SMeSs

SMeSs is a utility for syncing your text messages to a server, and then exposing them via a simple POST request JSON return API. This allows you to self-host a service (pairing it with a front-end) to be able to text from your desktop, see your messages, and so on.
