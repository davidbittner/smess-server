use mysql as my;

use std::sync::RwLock;
use config::Config;

use super::responses::ApiResponse;

use sha2::{
    Sha512Trunc256,
    Digest
};

use mysql::Conn;

use serde_json::Value;

///This is the format of query string expected on all auth-based requests.
#[derive(FromForm)]
pub struct AuthParameter {
    pub username: String,
    pub password: String
}

pub fn connect(config: &RwLock<Config>) -> Result<my::Conn, my::Error>
{
    let url = format!(
        "mysql://{}:{}@{}/{}",
        config.read().unwrap().get_str("database.username").unwrap(),
        config.read().unwrap().get_str("database.password").unwrap(),
        config.read().unwrap().get_str("database.url").unwrap(),
        //db name
        "smess",
    );

    my::Conn::new(url)
}

fn authenticate(conn: &mut my::Conn, username: &String, password: &String) -> Result<u32, ApiResponse> {
    let mut stmt = conn.prepare(
        "
        SELECT * FROM users
        WHERE username = :username
        "
    ).unwrap();

    let res: Option<(u32, String, Vec<u8>, Vec<u8>)> = stmt.first_exec(params!{
        "username" => username,
    }).unwrap();

    match res {
        Some((id, _, salt, hash)) => {
            let mut hasher = Sha512Trunc256::default();
            hasher.input(password.as_bytes());
            hasher.input(salt.as_slice());

            let hasher = hasher.result();

            if hasher.as_slice() == hash.as_slice() {
                Ok(id)
            }else{
                Err(ApiResponse{
                    msg:  "invalid password given".to_string(),
                    code: 1,

                    data: super::serde_json::Value::Null,
                })
            }
        },
        None => {
            Err(ApiResponse{
                msg:  format!("user: {} does not exist", username),
                code: 1,

                data: super::serde_json::Value::Null,
            })
        }
    }
}

///This function is used for attempting a connection, and then authenticating
///with the given credentials.
pub fn auth_and_connect(auth: AuthParameter, settings: &RwLock<Config>) -> Result<(Conn, u32), ApiResponse> {
    let mut conn = match connect(&settings) {
        Ok(conn) => conn,
        Err(err) => {
            return Err(ApiResponse{
                msg: err.to_string(),
                code: 1,

                data: Value::Null
            });
        }
    };

    match authenticate(&mut conn, &auth.username, &auth.password) {
        Ok(id)   => Ok((conn, id)),
        Err(err) => Err(err),
    }
}
