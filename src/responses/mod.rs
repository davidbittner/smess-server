use std::sync::RwLock;
use config::Config;

use mysql as my;
use mysql::chrono::{
    NaiveDateTime,
};

use super::auth::connect;

use sha2::{Sha512Trunc256, Digest};

use rand::{
    Rng,
    thread_rng
};

use rand::distributions::Standard;

#[derive(Serialize, Deserialize)]
pub struct TextMsg
{
    pub msg:    String,
    pub numbers: String,
    pub date:   String
}

#[derive(Serialize, Deserialize)]
pub struct Contact
{
    pub numbers:     String,

    pub first_name: String,
    pub last_name:  String
}

use serde_json::Value;
#[derive(Serialize, Deserialize)]
pub struct ApiResponse
{
    pub msg:  String,
    pub code: u32,

    pub data: Value,

}

pub fn retr_texts(conn: &mut my::Conn, user_id: u32, amount: u32) -> Result<Vec<TextMsg>, my::Error> {

    let results = conn.prep_exec(
        "SELECT * FROM messages WHERE user_key = :user_key LIMIT :amount;",
        params!{
            "user_key" => user_id,
            "amount"   =>  amount,
        }
    )?;

    let mut temp_results = Vec::new();
    for result in results {
        temp_results.push(result?);
    }

    let results: Vec<TextMsg> = temp_results.into_iter().map(|mut row| {
        let numbers: Option<String>    = row.take(2);
        let msg:     Option<String>    = row.take(3);
        let date:    Option<my::Value> = row.take(4);

        use mysql::prelude::*;

        let date: NaiveDateTime = match date {
            Some(date) => NaiveDateTime::from_value(date),
            //This should never happen
            None => panic!("date column cannot be null")
        };

        TextMsg{
            msg:    msg.unwrap(),
            numbers: numbers.unwrap(),
            date:   date.to_string()
        }
    }).collect();

    Ok(results)
}

pub fn add_message(mut conn: my::Conn, user_id: u32, msg: TextMsg) -> Result<(), my::Error>
{
    conn.prep_exec(
        "INSERT INTO messages VALUES (0, :user_key, :numbers, :msg, :date);",
        params!{
            "user_key" => user_id,
            "msg"      => msg.msg,
            "numbers"  => msg.numbers,
            "date"     => msg.date,
        }
    )?;

    Ok(())
}

pub fn create_user(config: &RwLock<Config>, username: String, password: String) -> Result<(), my::Error>
{
    let mut conn = connect(config)?;
    let mut hasher = Sha512Trunc256::default();

    let salt: Vec<u8> = 
        thread_rng()
            .sample_iter(&Standard)
            .take(32)
            .collect();

    hasher.input(password.as_bytes());
    hasher.input(salt.as_slice());

    let mut stmt = conn.prepare(
        "INSERT INTO users 
        (
            username,
            salt,
            hash
        )
        VALUES
        (
            :username,
            :salt,
            :password
        )"
    )?;

    stmt.execute(params!{
        "username" => username,
        "salt"     => salt,
        "password" => hasher.result().as_slice()
    })?;

    Ok(())
}

pub fn add_contact(mut conn: my::Conn, user_id: u32, cont: Contact) -> Result<(), my::Error>
{
    let mut stmt = conn.prepare(
        "INSERT INTO contacts VALUES
        (
            :user_id,
            :numbers,
            :first_name,
            :last_name
        )"
    )?;

    stmt.execute(params!{
        "user_id"    => user_id,
        "numbers"    => cont.numbers,
        "first_name" => cont.first_name,
        "last_name"  => cont.last_name
    })?;

    Ok(())
}
