#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;

extern crate phonenumber;

extern crate sha2;
extern crate rand;

#[macro_use]
extern crate mysql;

#[macro_use]
extern crate lazy_static;

extern crate config;

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

use rocket_contrib::Json;

use std::sync::RwLock;
use std::error::Error;
use config::Config;

lazy_static!{
    static ref SETTINGS: RwLock<Config> = RwLock::new(Config::default());
}

mod responses;
mod auth;

use auth::{
    auth_and_connect,
    AuthParameter
};


///This endpoint is used to retrieve the last <amount> texts from the
///given user.
#[post("/msgs/get/<amount>?<auth>")]
fn txts_num(amount: u32, auth: AuthParameter) -> Json<responses::ApiResponse>
{
    let (mut conn, user_id) = match auth_and_connect(auth, &SETTINGS) {
        Ok((conn, user_id)) => (conn, user_id),
        Err(msg)            => return Json(msg),
    };

    let texts = responses::retr_texts(&mut conn, user_id, amount);
    let texts = json!(match texts {
        Ok(txts) => txts,
        Err(err) => {
            return Json(
                responses::ApiResponse{
                    msg: err.to_string(),
                    code: 2,

                    data: serde_json::Value::Null,
                }
            );
        }
    });

    Json(responses::ApiResponse{
        msg: "success".to_string(),
        code: 0,

        data: texts
    })
}

///This endpoint is used to add a new message to the given user's collection.
#[post("/msgs/add?<auth>", data="<msg>")]
fn new_message(auth: AuthParameter, msg: Json<responses::TextMsg>) -> Json<responses::ApiResponse>
{
    let (conn, user_id) = match auth_and_connect(auth, &SETTINGS) {
        Ok((conn, user_id)) => (conn, user_id),
        Err(msg)            => return Json(msg),
    };

    use phonenumber::country::US;

    //Parse the phone number
    let mut msg = msg.into_inner();
    let number  = match phonenumber::parse(Some(US), &msg.numbers) {
        Ok(num)  => num,
        Err(err) => {
            return Json(
                responses::ApiResponse{
                    msg: err.to_string(),
                    code: 2,

                    data: serde_json::Value::Null,
                }
            );
        }
    };

    //Verify the number is valid
    if !phonenumber::is_valid(&number) {
        return Json(responses::ApiResponse{
            msg: format!("invalid number: {}", msg.numbers),
            code: 1,

            data: serde_json::Value::Null,
        });
    }

    //Format it into the E164 standard, no decorations
    //+1-222-2222 = +12222222
    msg.numbers = number.format().mode(phonenumber::Mode::E164).to_string();

    match responses::add_message(conn, user_id, msg)
    {
        Ok(_) => Json(
            responses::ApiResponse{
                msg:"success".to_string(),
                code: 0,
                
                data: serde_json::Value::Null
            }
        ),
        Err(err) => Json(
            responses::ApiResponse{
                msg: err.to_string(),
                code: 1,

                data: serde_json::Value::Null,
            }
        ),
    }
}

///This endpoint is used for adding a new contact to a given user.
///Contacts are only to be requested by various programs.
///Linking them to actual messages is up to the program's implementation.
#[post("/cont/add?<auth>", format="application/json", data="<contact>")]
fn new_contact(auth: AuthParameter, contact: Json<responses::Contact>) -> Json<responses::ApiResponse>
{
    let (conn, user_id) = match auth_and_connect(auth, &SETTINGS) {
        Ok((conn, user_id)) => (conn, user_id),
        Err(msg)            => return Json(msg),
    };

    match responses::add_contact(conn, user_id, contact.into_inner()) {
        Ok(_) => Json(
            responses::ApiResponse{
                code: 0,
                msg:  "success".to_string(),
                data: serde_json::Value::Null,
            }
        ),
        Err(err) => Json(
            responses::ApiResponse{
                code: 1,
                msg:  err.to_string(),
                data: serde_json::Value::Null,
            }
        )
    }
}

///This is the only non-auth-based request, it is used
///to create a new user profile with the given credentials.
#[post("/user/add?<auth>")]
fn new_user(auth: AuthParameter) -> Json<responses::ApiResponse>
{
    match responses::create_user(&SETTINGS, auth.username, auth.password) {
        Ok(_) => {
            Json(
                responses::ApiResponse{
                    msg: "success".to_string(),
                    code: 0,

                    data: serde_json::Value::Null
                }
            )
        },
        Err(err) => {
            Json(
                responses::ApiResponse{
                    msg: err.to_string(),
                    code: 1,

                    data: serde_json::Value::Null,
                }
            )
        }
    }
}

#[catch(404)]
fn not_found() -> Json<responses::ApiResponse> {
    Json(responses::ApiResponse{
        msg: "404, url was not valid".to_string(),
        code: 404,
        data: serde_json::Value::Null,
    })
}

fn main() -> Result<(), Box<Error>> {
    SETTINGS.write()?.merge(
        config::File::with_name("smess.toml"))?;

    rocket::ignite()
        .mount("", routes![
            txts_num,
            new_message,
            new_contact,
            new_user,
        ])
        .catch(catchers![
            not_found,
        ])
        .launch();

    Ok(())
}
